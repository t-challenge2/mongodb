FROM mongo:4.2.1

EXPOSE 27017/tcp

COPY ./initdb.js /docker-entrypoint-initdb.d/
